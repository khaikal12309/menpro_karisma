<?php

namespace App\Console\Commands;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;


class SendTaskEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:latetask';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tasklist telat';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $warning_date = Carbon::now()->addDays(3)->format('Y-m-d');
        $users = DB::table('task')
            ->join('tim', 'tim.id_tim', '=', 'task.id_tim')
            ->join('users', 'tim.id_user', '=', 'users.id')
            ->where('task.finish_date','<',$warning_date)
            ->get();
    
 
    foreach($users as $user) {

        // Send the email to user
       
        Mail::to($user->email)->send(new SendMailable($user));
 
    }
 
    $this->info('Task Massage Successfull');
    }
}
