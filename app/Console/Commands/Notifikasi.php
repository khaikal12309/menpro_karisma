<?php

namespace App\Console\Commands;
use App\Notif;
use Notification;
use \Illuminate\Notifications\Notifiable;
use Illuminate\Console\Command;
use Illuminate\Notifications\LateTask;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Notifikasi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    use Notifiable;
    protected $signature = 'notif:late';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show Notif when Task Late';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $warning_date = Carbon::now()->addDays(7)->format('Y-m-d');
        $users = DB::table('task')
            ->join('tim', 'tim.id_tim', '=', 'task.id_tim')
            ->join('users', 'tim.id_user', '=', 'users.id')
            ->where('task.finish_date','<',$warning_date)
            ->select('task.id as id_task','users.id as id_user','tim.id_tim','task.text','task.finish_date')
            ->get();
 
    foreach($users as $user) {

        // Send the email to user
        $notif = new Notif;
        $notif->type = $user->text;
        $notif->id_user = $user->id_user;
        $notif->id_tim = $user->id_tim;
        $notif->id_task =$user->id_task;
        $notif->finish_date =$user->finish_date;

        $notif->save();
 
    }
    $this->info('Task Notif Success');
    }
}
