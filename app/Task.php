<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $table = 'task';
    protected $appends = ["open"];
 
    public function getOpenAttribute(){
        return true;
    }

    public function tim(){
        return $this->belongsTo('App\Tim','id_tim');
    
    }
    
    
}
