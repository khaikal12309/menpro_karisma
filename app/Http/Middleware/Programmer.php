<?php

namespace App\Http\Middleware;
use Auth;
use App\Proyek;
use App\User;
use App\Tim;
use Alert;
use Closure;

class Programmer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user= Auth::user()->id;
        $tim = Tim::where('id_user',$user)->where('jabatan','programmer')->get();
        if (count($tim) >= 1){
            return $next($request);         
        }
        Alert::error('Kamu tidak punya akses kesini','Opps !');
        return back();
    }
}
