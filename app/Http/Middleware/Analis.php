<?php

namespace App\Http\Middleware;
use Auth;
use App\Proyek;
use App\User;
use App\Tim;
use Closure;
use Alert;

class Analis
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user= Auth::user()->id;
        $tim = Tim::where('id_user',$user)->where('jabatan','analis')->get();
        if (count($tim) >= 1){
            return $next($request);         
        }
        Alert::error('Kamu tidak punya akses kesini','Opps !');
        return back();
    }
    
}
