<?php

namespace App\Http\Middleware;
use Auth;
use App\Proyek;
use Alert;
use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user()->id;
        $id_proyek = $request->get('id_proyek');
       
        $proyek = Proyek::where('id_pm',$user)->orWhere('id_client',$user)->first();
        $id_client = $proyek['id_client'];
        $id_pm =$proyek['id_pm'];
        // dd($request);
        if(Auth::check() && Auth::user()->isAdmin()){
            return $next($request);
        }elseif($user == $id_client){
            return redirect('homemaster');
        }elseif($user == $id_pm){
            return redirect('homemaster');
        }
        return redirect('home');
    }
}
