<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Proyek;
use App\User;
use App\Tim;
use Closure;

class cek
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = Auth::user()->id;
        $id_proyek = $request->get('id_proyek');
       
        $proyeks = Proyek::where('id_pm','=',$user)->first();
        $proyek = Proyek::where('id_proyek',$id_proyek)->first();
        $id_pm = $proyek['id_pm'];
        $id_client = $proyek['id_client'];
        $tim = Tim::where('id_user','=',$user)->where('id_proyek','=',$id_proyek)->get();
        $proyeksi = DB::table('tim')
            ->join('proyek', 'tim.id_proyek', '=', 'proyek.id_proyek')
            ->join('users', 'tim.id_user', '=', 'users.id')
            ->where('proyek.id_proyek',$id_proyek)
            ->Where('tim.id_user',$user)
            ->select('tim.jabatan','proyek.id_proyek')
            ->first();
        $jabatan =$proyeksi->jabatan;
       if($jabatan == "analis"){
            return redirect()->route('Analishome', $proyek) ;
        }elseif($jabatan == "programmer"){
            return redirect()->route('Programmerhome', $proyek) ;
        }elseif($jabatan == "tester"){
            return redirect()->route('Testerhome', $proyek) ;
        }
        return back()->withInput();
    }
}
