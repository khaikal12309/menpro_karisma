<?php

namespace App\Http\Middleware;
use Auth;
use App\Proyek;
use App\User;
use Closure;

class Pm
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user()->id;
        $proyek = Proyek::where('id_pm','=',$user)->first();
       //dd($request);
        //dd($proyek->id_pm); 
       if($user == $proyek->id_pm){
            return $next($request);
        }
        return back()->withInput();
    }
}
