<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Proyek;
use Alert;
class Client
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user()->id;
        $proyek = Proyek::where('id_client',$user)->first();
        $id_client = $proyek['id_client'];
       
        if($user == $id_client){
            return $next($request);
        }
        Alert::error('Kamu tidak punya akses kesini','Opps !');
        return back();
    }
}
