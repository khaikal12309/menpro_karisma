<?php

namespace App\Http\Middleware;
use Auth;
use App\Proyek;
use Alert;
use Closure;

class Cekmaster
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user()->id;
        $id_proyek = $request->get('id_proyek');
       
        $proyek = Proyek::where('id_pm',$user)->orWhere('id_client',$user)->first();
        $id_client = $proyek['id_client'];
        $id_pm =$proyek['id_pm'];
        if($id_client == $user){
            Alert::success('Selamat Datang '.Auth::user()->name,'Hallo !');
            return redirect()->route('Clienthome', $id_proyek) ;
        }elseif($id_pm == $user){
            Alert::success('Selamat Datang '.Auth::user()->name,'Hallo !');
            return redirect()->route('Pmhome', $id_proyek) ;
        }
        Alert::error('Kamu tidak punya akses kesini','Oopps !');
        return back();
    }
}
