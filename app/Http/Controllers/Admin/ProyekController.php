<?php

namespace App\Http\Controllers\Admin;
use App\Proyek;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Alert;

class ProyekController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proyeks = Proyek::all();
        $users =User::all();
        //dd($users);
        //dd($proyeks->user);
        return view ('admin.proyek', compact('proyeks','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_proyek' => 'required',
            'client'=> 'required',
            'pm' => 'required',
            //'status' => 'required'
          ]);

        $proyek = new Proyek;
        $proyek->nama_proyek = $request->get('nama_proyek');
        $proyek->id_pm = $request->get('pm');
        $proyek->id_client =  $request->get('client');
        $proyek->save();
        Alert::success('Data proyek berhasil ditambahkan','Selamat !')->persistent("Close");
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_proyek)
    {
        $request->validate([
            'nama_proyek' => 'required',
            'client'=> 'required',
            'pm' => 'required',
            //'status' => 'required'
          ]);

        $proyek = Proyek::find($id_proyek);
        $proyek->nama_proyek = $request->get('nama_proyek');
        $proyek->id_pm = $request->get('pm');
        $proyek->id_client =  $request->get('client');
        $proyek->save();
        Alert::success('Data proyek berhasil diubah','Selamat !')->persistent("Close");
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_proyek)
    {
        $proyek = Proyek::find($id_proyek);
        $proyek->delete();
        Alert::success('Data proyek berhasil dihapus','Selamat !')->persistent("Close");
        return back();
    }
}
