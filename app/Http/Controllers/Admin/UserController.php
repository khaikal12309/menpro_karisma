<?php

namespace App\Http\Controllers\Admin;
use  App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Crypt;
use Alert;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view ('admin.user', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.adduser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:4|max:20',
            'email'=> 'required|unique:users',
            'password' => 'required',
            //'status' => 'required'
          ]);

        $user = new User;
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->status= '';
        $user->password =  Hash::make($request->get('password'));
        $user->save();
        Alert::success('Data user berhasil ditambahkan','Selamat !')->persistent("Close");
        return back();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
            'name' => 'required|min:4|max:20',
            'email'=> 'required',
            //'status' => 'required'
          ]);

        
        $user = User::find($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->save();
        Alert::success('Data user berhasil diubah','Selamat !')->persistent("Close");
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        Alert::success('Data user berhasil dihapus','Selamat !')->persistent("Close");
        return back();
    }

    public function changepassword()
    {
        $User = User::find(Auth::user()->id);
  
  
        if(Hash::check(Input::get('current_password'), $User['password'] ) && Input::get('password') == Input::get('password_confirmation')){
          $User->password = bcrypt(Input::get('password'));
          $User->save();
        //   Alert::message('Robots are working!');
        Alert::success('Password berhasli diganti', 'Selamat!');
          return back();
        }
        else {
        Alert::error('Mungkin password anda salah', 'Oppss!');
          return back();
        }
  
      }
}
