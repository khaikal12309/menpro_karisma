<?php

namespace App\Http\Controllers\Programmer;

use Illuminate\Http\Request;
use App\Dokumen;
use App\Http\Controllers\Controller;
use Alert;
use Carbon\Carbon;

class DokumenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $dokumens = Dokumen::where('id_proyek',$id)->get();
        $id_proyek=$id;
        if(count($dokumens) > 0){
        
        return view('programmer.dokumen',compact('dokumens','id_proyek'));
        }else{
            
        Alert::error('Tidak ada data','Oopss !')->persistent("Close");
        return view('programmer.dokumen',compact('dokumens','id_proyek'));
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tgl_upload= Carbon::now()->format('Y-m-d');

        $file = $request->file('file');
        $ext = $file->getClientOriginalExtension();
        $newName = rand(100000,1001238912).".".$ext;
        $file->move('uploads/file',$newName);

        $dokumen = new Dokumen;
        $dokumen->nama_dokumen = $request->get('nama_dokumen');
        $dokumen->id_proyek = $request->get('id_proyek');
        $dokumen->tgl_upload = $tgl_upload;
        $dokumen->file = $newName;
        $dokumen->save();

        Alert::success('Dokumen berhasil ditambahkan','Sukses !')->persistent("Close");
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $dokumen = Dokumen::find($id);

        if($request->file('file') == "")
        {
            $dokumen->file = $dokumen->file;
        } 
        else
        {
            $file       = $request->file('file');
            $fileName   = $file->getClientOriginalName();
            $request->file('file')->move('uploads/file', $fileName);
            $dokumen->file = $fileName;
        }
        // $file = $request->file('file');
        // $ext = $file->getClientOriginalExtension();
        // $newName = $request->file.".".$ext;
        // $file->move('uploads/file',$newName);

        $dokumen->nama_dokumen = $request->get('text');
        
        $dokumen->save();

        Alert::success('Dokumen berhasil diubah','Sukses !')->persistent("Close");
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dokumen = Dokumen::find($id);
        $dokumen->delete();

        Alert::success('Dokumen berhasil dihapus','Sukses !')->persistent("Close");
        return back();
    }
    public function download($id_dokumen)
    {
        //$model_file = Model::findOrFail($id_dokumen);
        $book = Dokumen::where('id_dokumen', $id_dokumen)->firstOrFail();
        $pathToFile = ('uploads/file/'.$book->file);
        $name=$book->nama_dokumen.'.docx';
        return response()->download($pathToFile,$name);
    }
}
