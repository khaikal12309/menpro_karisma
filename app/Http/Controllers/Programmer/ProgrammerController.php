<?php

namespace App\Http\Controllers\Programmer;
use Auth;
use App\Tim;
use App\Task;
use Alert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProgrammerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_proyek)
    {
        $id_proyek = $id_proyek;
        $id_user = Auth::user()->id;
        $tim = Tim::where('id_proyek',$id_proyek)->where('id_user',$id_user)->select('id_tim')->first();
        $id_tim = $tim->id_tim;
        $tasks = Task::where('id_proyek','=',$id_proyek)->where('id_tim',$id_tim)->selectRaw('sum(harga) as gaji')->first();
        $tasks2 = Task::where('id_proyek','=',$id_proyek)->where('id_tim',$id_tim)->get();
        $totaltask = count($tasks2);
        $gaji = $tasks['gaji'];
        $hasil_rupiah = "Rp " . number_format($gaji,2,',','.');

        Alert::info('Selamat Datang Programmer','Hello !');
        return view('programmer.index',compact('id_proyek','gaji','hasil_rupiah','totaltask'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function timeline($id_proyek){
        $id_proyek = $id_proyek;
        return view('programmer.timeline',compact('id_proyek'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
