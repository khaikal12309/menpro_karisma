<?php

namespace App\Http\Controllers;
use App\User;
use Carbon;
use Illuminate\Support\Facades\Auth;
use App\Proyek;
use Illuminate\Http\Request;

class PmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function proyek(Request $request)
    {
        $tgl_sekarang = Carbon\Carbon::now();
        $id_user = Auth::user() ->id;
        $users = User::all();
        $proyeks = Proyek::all()->where('id_pm','=',$id_user);
        return view('pm.index',compact('users','proyeks','tgl_sekarang'));
    }
    
    public function index($id_proyek)
    {
        $proyeks = Proyek::where('id_proyek','=',$id_proyek);
        $id_proyek = $id_proyek;
        return view('pm.index',compact('proyeks','id_proyek'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function gant($id_proyek){
        $id_proyek = $id_proyek;
        return view('pm.timeline',compact('id_proyek'));
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tgl_sekarang = Carbon\Carbon::now();
        $id_proyek= $request->proyek;
        $id_user = Auth::user() ->id;
        $users = User::all();
        $proyeks = Proyek::all()->where('id_pm','=',$id_user);
        return view('pm.index',compact('users','proyeks','tgl_sekarang','id_proyek'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
