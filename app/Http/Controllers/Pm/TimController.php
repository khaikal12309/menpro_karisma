<?php

namespace App\Http\Controllers\Pm;
use App\User;
use App\Tim;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class TimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_proyek)
    {  
        $id_pm = DB::table('proyek')->select('id_pm')->where('id_proyek',$id_proyek)->first();
        $id_client = DB::table('proyek')->select('id_client')->where('id_proyek',$id_proyek)->first();
      
        $tims = Tim::where('id_proyek','=',$id_proyek)->get();
        
        $users = DB::table('users')->whereNotIn('id', function($r) use ($id_proyek){
            $r->select('id_user')->from('tim')->where('id_proyek',$id_proyek);
        })->get();

        
        
        
        
        return view('pm/tim', compact('tims','id_proyek','users','id_pm','id_client'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tim = new Tim();
        $tim->id_user = $request->get('id_user');
        $tim->jabatan = $request->get('jabatan');
        $tim->id_proyek = $request->get('id_proyek');
        $tim->save();
        return back()->with('success','Data Tim ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_tim)
    {
        $tim = Tim::find($id_tim);
        $tim->id_user = $request->get('id_user');
        $tim->jabatan = $request->get('jabatan');
        $tim->save();
        return back()->with('success','Data Tim telah diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_tim)
    {
        $tim = Tim::find($id_tim);
        $tim->delete();
        return back()->with('success','Data Tim telah dihapus');

    }
}
