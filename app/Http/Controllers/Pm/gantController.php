<?php

namespace App\Http\Controllers\Pm;
use App\Link;
use App\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class gantController extends Controller
{
    public function get($id_proyek){
        $tasks = new Task();
        $links = new Link();
 
        return response()->json([
            "data" => $tasks->where('id_proyek','=',$id_proyek)->get(),
            "links" => $links->all()
        ]);
    }
}
