<?php

namespace App\Http\Controllers\Analis;
use Alert;
use App\Task;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AnalisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_proyek)
    {
        $user = Auth::user()->name;
        Alert::info('Selamat Datang ' .$user,'Hello !');
        return view('analis.index',compact('id_proyek'));
    }

    public function mytask($id_proyek)
    {
        $user = Auth::user()->name;
        $mytasks = Task::where('id_proyek',$id_proyek)->where('id_analis','<>',NULL)->get();
        return view('analis.mytask',compact('mytasks','id_proyek'));
    }

    public function mytaskupdate($id_task)
    {
        $id_proyek = $request->get('id_proyek');
        $task = Task::find($id_task);
        $task->status_analis = $request->get('status_analis');

        Alert::success('Sukses','Selamat!');
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function timeline($id_proyek){
        $id_proyek = $id_proyek;
        return view('analis.timeline',compact('id_proyek'));
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
