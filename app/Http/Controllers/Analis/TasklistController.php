<?php


namespace App\Http\Controllers\Analis;


use App\Http\Controllers\Controller;
use App\Task;
use App\Tim;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Notif;
use Alert;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TasklistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $warning_date = Carbon::now()->addDays(7)->format('Y-m-d');
        // $users = DB::table('task')
        //     ->join('tim', 'tim.id_tim', '=', 'task.id_tim')
        //     ->join('users', 'tim.id_user', '=', 'users.id')
        //     ->where('task.finish_date','<',$warning_date)
        //     ->select('task.id as id_task','users.id as id_user','tim.id_tim','task.text','task.finish_date')
        //     ->get();
        //     dd($users);
        $tgl_sekarang= Carbon::now()->formatLocalized('%A, %d %B %Y');
        $warning_date = Carbon::now()->addDays(7)->format('Y-m-d');
        $notifikasi =Notif::where('id_user','=',Auth::user()->id);
        $id_proyek = $id;
        $tasks = Task::where('id_proyek','=',$id)->get();
      
        $programmers = Tim::where('id_proyek','=',$id)->where('jabatan','programmer')->get();
        if (count($tasks) <= 0){
            Alert::error('Tidak ada data','Oopss !');
            return view('analis.task',compact('tasks','id_proyek','programmers','warning_date','tgl_sekarang'));   
        }
        return view('analis.task',compact('tasks','id_proyek','programmers','warning_date','tgl_sekarang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $start_date = $request->get('start_date');// pendefinisian tanggal awal
        $duration = $request->get('duration');
        $finish = date('Y-m-d', strtotime('+'.$duration.' days', strtotime($start_date))); 

        $task = New Task();
        $task->text = $request->get('text');
        $task->duration = $duration;
        $task->start_date = $start_date;
        $task->finish_date = $finish;  
        $task->id_proyek = $request->get('id_proyek');  
        $task->id_tim = $request->get('id_tim');
        $task->status = '';
        $task->status_deployprogrammer = '' ;
        $task->status_deploypm = '';
        $task->save();


        Alert::success('Data tasklist berhasil ditambahkan','Selamat !')->persistent("Close");
        return back();

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $task=Task::find($id);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $analis_update = $request->get('status_analis');
        if($analis_update != NULL or $analis_update != ''){
            $task = Task::find($id);
            $task->status_analis = $request->get('status_analis');
            $task->save();
    
            Alert::success('Data tasklist berhasil diubah','Selamat !')->persistent("Close");;
            return back();
        }else{
        $start_date = $request->get('start_date');// pendefinisian tanggal awal
        $duration = $request->get('duration');
        $finish = date('Y-m-d', strtotime('+'.$duration.' days', strtotime($start_date))); 
        
        $task = Task::find($id);
        $task->text = $request->get('text');
        $task->duration = $duration;
        $task->start_date = $start_date; 
        $task->finish_date = $finish;
        $task->id_proyek = $request->get('id_proyek');  
        $task->id_tim = $request->get('id_tim');
        $task->save();

        Alert::success('Data tasklist berhasil diubah','Selamat !')->persistent("Close");;
        return back();
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);
        $task->delete();

        Alert::success('Data tasklist berhasil dihapus','Selamat !')->persistent("Close");;
        return back();
    }
}
