<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\User;
use Carbon;
use Illuminate\Support\Facades\Auth;
use App\Proyek;
use Illuminate\Http\Request;
use PDF;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tgl_sekarang = Carbon\Carbon::now();
        $id_user = Auth::user() ->id;
        $users = User::all();
        $proyeks = DB::table('tim')
            ->join('proyek', 'tim.id_proyek', '=', 'proyek.id_proyek')
            ->join('users', 'tim.id_user', '=', 'users.id')
            ->where('proyek.id_pm',$id_user)
            ->orWhere('tim.id_user',$id_user)
            ->orWhere('proyek.id_client',$id_user)
            ->get();
        return view('home',compact('users','proyeks','tgl_sekarang'));
    }

    public function master()
    {
        $tgl_sekarang = Carbon\Carbon::now();
        $id_user = Auth::user() ->id;
        $users = User::all();
        $proyeks = DB::table('proyek')
            ->where('proyek.id_pm',$id_user)
            ->orWhere('proyek.id_client',$id_user)
            ->get();
        return view('homemaster',compact('users','proyeks','tgl_sekarang'));
    }

    public function salary(){
        $id_user = Auth::user() ->id;
        $salarys = DB::table('task')
        ->join('tim', 'task.id_tim', '=', 'tim.id_tim')
        ->join('proyek','proyek.id_proyek','=','task.id_proyek')
        ->where('tim.id_user','=',$id_user)
        ->select('proyek.nama_proyek',DB::raw('sum(harga) as gaji'))
        ->groupBy('proyek.nama_proyek')
        ->get();
        return view('homesalary',compact('salarys'));
    }

    public function cetak_pdf(){
        $id_user = Auth::user() ->id;
        $salarys = DB::table('task')
        ->join('tim', 'task.id_tim', '=', 'tim.id_tim')
        ->join('proyek','proyek.id_proyek','=','task.id_proyek')
        ->where('tim.id_user','=',$id_user)
        ->select('proyek.nama_proyek',DB::raw('sum(harga) as gaji'))
        ->groupBy('proyek.nama_proyek')
        ->get();
 
	    $pdf = PDF::loadview('salary_pdf',compact('salarys'));
	    return $pdf->download('laporan-salary');
    }

    
}
