<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tim extends Model
{
public $timestamps = false;
  protected $primaryKey = 'id_tim';
  protected $table = 'tim';
  protected $fillable = [
      'id_proyek','id_user','jabatan'
  ];

  public function karyawan()
    {
        return $this->belongsTo('App\User','id_user');
    }
    public function tim()
    {
        return $this->hasMany('App\Task');
    }
    
}
