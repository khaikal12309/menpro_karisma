<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyek extends Model
{
public $timestamps = false;
  protected $primaryKey = 'id_proyek';
  protected $table = 'proyek';
  protected $fillable = [
      'id_proyek','nama_proyek','id_tim', 'status'
  ];


  public function pm()
    {
        return $this->belongsTo('App\User','id_pm');
    }
    public function client()
    {
        return $this->belongsTo('App\User','id_client');
    }
}
