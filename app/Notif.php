<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notif extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $table = 'notifications';
    protected $appends = ["open"];
 
    public function getOpenAttribute(){
        return true;
    }
}
