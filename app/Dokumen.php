<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dokumen extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'id_dokumen';
    protected $table = 'dokumen';
    protected $appends = ["open"];
 
    public function getOpenAttribute(){
        return true;
    }
}
