<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


   // Route::resource('/','PmController@proyek');
    Route::get('/data/{id_proyek}','Pm\gantController@get');
    Route::resource('task', 'Pm\TaskController');
    Route::resource('link', 'Pm\LinkController');
    //Route::get('coba/{id}','AdminController@show');
