<?php

use Illuminate\Support\Facades\Hash;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Auth::routes();
Route::get('/coba','CobaController@coba');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/homesalary', 'HomeController@salary')->name('home');
Route::get('/homesalary/cetak_pdf', 'HomeController@cetak_pdf')->name('home');
Route::get('/homemaster', 'HomeController@master')->name('homepm');

Route::post('/home/change_password','Admin\UserController@changepassword');
Route::group(['prefix'=> 'client,', 'middleware' => ['auth']], function(){
    Route::resource('/','HomeController@index');
    //Route::get('coba/{id}','AdminController@show');
});
//tim
Route::post('/proyek/{id_proyek}','HomeController@cekstatus')->middleware('cek','auth');
//pm & client
Route::post('/master/{id_proyek}','HomeController@cekstatus')->middleware('cekmaster','auth');

Route::group(['prefix'=> 'pm', 'middleware' => ['pm','auth']], function(){
    Route::get('/{id_proyek}','PmController@index')->name('Pmhome');
    Route::get('/tasklist/{id_proyek}','TasklistController@index');
    Route::resource('Task','TasklistController');
    Route::get('/timeline/{id_proyek}','PmController@gant');
    Route::get('/tim/{id_proyek}','Pm\TimController@index');
    Route::resource('Tim','Pm\TimController');
    Route::get('/dokumen/{id_proyek}','Pm\DokumenController@index');
    Route::resource('Dokumen','Pm\DokumenController');
    Route::get('dokumen/{id_dokumen}/download', 'Pm\DokumenController@download')->name('dokumen.download');
    //Route::get('tim/{id}','AdminController@show');
});

//client
Route::group(['prefix'=> 'client', 'middleware' => ['client','auth']], function(){
    Route::get('/{id_proyek}','Client\ClientController@index')->name('Clienthome');
    Route::get('/tasklist/{id_proyek}','Client\TasklistController@index');
    Route::resource('ClientTask','Client\TasklistController');
    //Route::get('coba/{id}','AdminController@show');
});

Route::group(['prefix'=> 'tester', 'middleware' => ['tester','auth']], function(){
    Route::get('/{id_proyek}','Tester\TesterController@index')->name('Testerhome');
    Route::get('/tasklist/{id_proyek}','Tester\TasklistController@index');
    Route::resource('TesterTask','Tester\TasklistController');
    //Route::get('coba/{id}','AdminController@show');
});

Route::group(['prefix'=> 'programmer', 'middleware' => ['programmer','auth']], function(){
    Route::get('/{id_proyek}','Programmer\ProgrammerController@index')->name('Programmerhome');
    //tasklist
    Route::get('/tasklist/{id_proyek}','Programmer\TasklistController@index');
    Route::resource('ProgrammerTask','Programmer\TasklistController');

    //timeline
    Route::get('/timeline/{id_proyek}','Programmer\ProgrammerController@timeline')->name('Programmerhome');
    //dokumen
    Route::get('/dokumen/{id_proyek}','Programmer\DokumenController@index');
    Route::resource('PenulisDokumen','Programmer\DokumenController');
    Route::get('dokumen/{id_dokumen}/download', 'Programmer\DokumenController@download')->name('programmerdokumen.download');
    //Route::get('coba/{id}','AdminController@show');
});

Route::group(['prefix'=> 'analis', 'middleware' => ['analis','auth']], function(){
    Route::get('/{id_proyek}','Analis\AnalisController@index')->name('Analishome');
    //tasklist
    Route::get('/tasklist/{id_proyek}','Analis\TasklistController@index');
    Route::resource('AnalisTask','Analis\TasklistController');
    //timeline
    Route::get('/timeline/{id_proyek}','Analis\AnalisController@timeline');

    //myTask
    Route::get('/mytask/{id_proyek}','Analis\AnalisController@mytask');
    //myTask
    Route::PUT('/mytaskupdate/{id_task}','Analis\AnalisController@mytaskupdate');
    //dokumen
    Route::get('/dokumen/{id_proyek}','Analis\DokumenController@index');
    Route::resource('AnalisDokumen','Analis\DokumenController');
    Route::get('dokumen/{id_dokumen}/download', 'Analis\DokumenController@download')->name('analisdokumen.download');
    //Route::get('coba/{id}','AdminController@show');
});
Route::group(['prefix'=> 'admin', 'middleware' => ['admin','auth']], function(){
    Route::resource('/','AdminController');
    Route::resource('User','Admin\UserController');
    Route::resource('Proyek','Admin\ProyekController');
   
});

//auth
Route::get('auth/{provider}', 'AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'AuthController@handleProviderCallback');