<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Data Tables</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{url('assets/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{url('assets/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{url('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{url('assets/dist/css/AdminLTE.min.css')}}">
   <!-- Select2 -->
   <link rel="stylesheet" href="{{url('assets/bower_components/select2/dist/css/select2.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{url('assets/dist/css/skins/_all-skins.min.css')}}">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  @include('admin.header')
  <!-- Left side column. contains the logo and sidebar -->
   <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{url('assets/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="">
          <a href="/admin">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>   
        </li>

        <li class="">
          <a href="/admin/User">
            <i class="fa fa-group"></i> <span>Kelola Users</span>
          </a>   
        </li>

        <li class="active">
          <a href="/admin/Proyek">
            <i class="fa fa-tasks"></i> <span>Kelola Proyek</span>
          </a>   
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Proyek
        <small>Manajemen Proyek Karisma</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li class="active">Data Proyek</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
     
        <div class="col-xs-12">
          <!-- /.box -->
          <div class="box">
            <div class="box-header">
                <button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#tambahdata">Tambah Data</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nama Proyek</th>
                  <th>Projek Manager</th>
                  <th>Client</th>
                  <th>Id Tim</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($proyeks as $proyek)
                <tr>
                  <td>{{$proyek->nama_proyek}}</td>
                  <td>{{$proyek->pm->name}}</td>
                  <td>{{$proyek->client->name}}</td>
                  <td></td>
                  <td>{{$proyek->status}}</td>
                  <td>
                  <form class="delete" action="{{ route('Proyek.destroy', $proyek->id_proyek)}}" method="post">
                  <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#{{$proyek->id_proyek}}">Edit</button>
                    
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                  </form>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- Modal -->
@foreach ($proyeks as $proyek)
<div id="{{$proyek->id_proyek}}" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal Edit data Proyek-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Proyek</h4>
      </div>
      <div class="modal-body">
        <form  method="POST" action="{{route('Proyek.update', $proyek->id_proyek)}}">
          @method('PATCH')
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Proyek</label>
                  <input type="text" name="nama_proyek" class="form-control" id="exampleInputName" value= "{{$proyek->nama_proyek}}" required>
                </div>
                <div class="form-group">
                  <label>Client</label>
                    <select name="client"  class="form-control select2" style="width: 100%;">
                      <option selected="selected" value="{{$proyek->client->id}}">{{$proyek->client->name}}</option>
                        @foreach ($users as $user)
                          @if ($user->status == "client" && $user->name != $proyek->client->name)
                            <option name="client" value="{{$user->id}}">{{$user->name}}</option>
                          @endif
                       @endforeach  
                    </select>
                </div>
                
                <div class="form-group">
                  <label>Project Manager</label>
                    <select name="pm" class="form-control select2" style="width: 100%;">
                      <option selected="selected" value="{{$proyek->pm->id}}">{{$proyek->pm->name}}</option>
                        @foreach ($users as $user)
                          @if ($user->status == "" && $user->name != $proyek->pm->name)
                            <option name="pm" value="{{$user->id}}">{{$user->name}}</option>
                          @endif
                       @endforeach  
                    </select>
                  </div>
                
              </div>
             
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Simpan</button>
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
        </form>
      </div>
    </div>

  </div>
</div>
@endforeach



<div id="tambahdata" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal tambah data content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Proyek</h4>
      </div>
      <div class="modal-body">
        <form  method="POST" action="{{route('Proyek.store')}}">
          @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Proyek</label>
                  <input type="text" name="nama_proyek" class="form-control" id="exampleInputName" required>
                </div>
                <div class="form-group">
                  <label>Client</label>
                    <select name="client"  class="form-control select2" style="width: 100%;">
                      <option selected="selected">Pilih Client</option>
                        @foreach ($users as $user)
                          @if ($user->status == "client")
                            <option name="client" value="{{$user->id}}">{{$user->name}}</option>
                          @endif
                       @endforeach  
                    </select>
                </div>
                
                <div class="form-group">
                  <label>Project Manager</label>
                    <select name="pm" class="form-control select2" style="width: 100%;">
                      <option selected="selected">Pilih Project Manager</option>
                        @foreach ($users as $user)
                          @if ($user->status !== "client")
                            <option name="pm" value="{{$user->id}}">{{$user->name}}</option>
                          @endif
                       @endforeach  
                    </select>
                  </div>
                
              </div>
              
          
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Simpan</button>
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
        </form>
      </div>
    </div>

  </div>
</div>


<!-- jQuery 3 -->
<script src="{{url('assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- DataTables -->
<script src="{{url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{url('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{url('assets/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- Select2 -->
<script src="{{url('assets/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{url('assets/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{url('assets/dist/js/demo.js')}}"></script>
<!-- page script -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@include('sweet::alert')
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script>
    $(".delete").on("submit", function(){
        return confirm("Do you want to delete this item?");
    });
</script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  })
</script>
</body>
</html>
