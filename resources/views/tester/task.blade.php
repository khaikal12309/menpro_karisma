<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Data Tables</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{url('assets/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{url('assets/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{url('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{url('assets/dist/css/AdminLTE.min.css')}}">
   <!-- Select2 -->
   <link rel="stylesheet" href="{{url('assets/bower_components/select2/dist/css/select2.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{url('assets/dist/css/skins/_all-skins.min.css')}}">
  
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  @include('admin.header')
  <!-- Left side column. contains the logo and sidebar -->
   <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{url('assets/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

       <!-- sidebar menu: : style can be found in sidebar.less -->
       <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="">
          <a href="/tester/{{$id_proyek}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>   
        </li>

        <li class="active">
          <a href="/tester/tasklist/{{$id_proyek}}">
            <i class="fa fa-tasks"></i> <span>Tasklist</span>
          </a>   
        </li>


      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data tim
        <small>Manajemen tim Karisma</small>
      </h1>
      <ol class="breadcrumb">
      <p>{{$tgl_sekarang}}</p>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
     
        <div class="col-xs-12">
          <!-- /.box -->
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Task</th>
                  <th>Status Deploy Programmerth</th>
                  <th>Status Deploy Project Manager</th>
                  <th>Status Test</th>
                  <!-- <th>S.Programmer</th>
                  <th>S.Pm</th> -->
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($tasks as $task)
                @if($task->finish_date <= $warning_date)
                <tr style="background-color:yellow;">
                  <td>{{$task->text}}</td>
                  <td>
                      @if($task->status_deployprogrammer == '' or $task->status_deployprogrammer == NULL)
                      <button class= 'btn btn-warning btn-sm btn-flat'>Belum Deploy</button>
                      @else
                      <button class= 'btn btn-success btn-sm'>{{$task->status_deployprogrammer}}</button>
                      @endif
                  </td>
                  <td>
                      @if($task->status_deploypm == '' or $task->status_deploypm == NULL)
                      <button class= 'btn btn-warning btn-sm'>Belum Deploy</button>
                      @else
                      <button class= 'btn btn-success btn-sm'>{{$task->status_deploypm}}</button>
                      @endif
                  </td>
                  <td>
                      @if($task->status == '' or $task->status == NULL)
                      <button class= 'btn btn-warning btn-sm'>Belum Deploy</button>
                      @elseif($task->status == 'No')
                      <button class= 'btn btn-danger btn-sm'>Error</button>
                      @else
                      <button class= 'btn btn-success btn-sm'>{{$task->status}}</button>
                      @endif
                  </td>
                  
                  <td>
                  
                  <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#status{{$task->id}}">Status</button>
                  
                 
                  </td>
                </tr>
                @else
                <tr>
                  <td>{{$task->text}}</td>
                  <td>
                      @if($task->status_deployprogrammer == '' or $task->status_deployprogrammer == NULL)
                      <button class= 'btn btn-warning btn-sm'>Belum Deploy</button>
                      @else
                      <button class= 'btn btn-success btn-sm'>{{$task->status_deployprogrammer}}</button>
                      @endif
                  </td>
                  <td>
                      @if($task->status_deploypm == '' or $task->status_deploypm == NULL)
                      <button class= 'btn btn-warning btn-sm'>Belum Deploy</button>
                      @else
                      <button class= 'btn btn-success btn-sm'>{{$task->status_deploypm}}</button>
                      @endif
                  </td>
                  <td>
                      @if($task->status == '' or $task->status == NULL)
                      <button class= 'btn btn-warning btn-sm'>Belum Deploy</button>
                      @elseif($task->status == 'No')
                      <button class= 'btn btn-danger btn-sm'>Error</button>
                      @else
                      <button class= 'btn btn-success btn-sm'>{{$task->status}}</button>
                      @endif
                  </td>
                  
                  <td>
               
                  <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#status{{$task->id}}">Status</button>
                  
                  
                  </td>
                </tr>
                @endif
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

<!-- Modal tambah edit status-->
@foreach($tasks as $task)
<div id="status{{$task->id}}" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal tambah data content-->
  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Status</h4>
      </div>
      <div class="modal-body">
        <form  method="POST" action="{{route('TesterTask.update', $task->id)}}">
          @method('PATCH')
          @csrf
              <div class="box-body">
                
              <div class="form-group">
                  <label>Status Test</label>
                  @if ($task->status_deployprogrammer !== 'Yes' or $task->status_deploypm !== 'Yes')
                  <select class="form-control" name="status" disabled>
                  <option value="{{$task->status}}" selected="selected">
                  @if($task->status == "")
                    Belum Test
                  @else
                    {{$task->status}}
                  @endif
                  </option>
                    <option value="Done">Done</option>
                    <option value="No">No</option>
                  </select>

                  @else
                  <select class="form-control" name="status">
                  <option value="{{$task->status}}" selected="selected">
                  @if($task->status == "")
                    Belum Test
                  @else
                    {{$task->status}}
                  @endif
                  </option>
                    <option value="Done">Done</option>
                    <option value="No">No</option>
                  </select>
                  @endif
                </div>
              </div>
         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
      </div>
    </div>

  </div>
</div>
@endforeach



<!-- jQuery 3 -->
<script src="{{url('assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- DataTables -->
<script src="{{url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{url('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{url('assets/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- Select2 -->
<script src="{{url('assets/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{url('assets/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{url('assets/dist/js/demo.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@include('sweet::alert')
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  })
</script>
</body>
</html>
