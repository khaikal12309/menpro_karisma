<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('task')->insert([
            ['id_task'=>1, 'nama_task'=>'Project #1', 'tgl_mulai'=>'2017-04-01 00:00:00', 
                'durasi'=>5, 'progress'=>0.8, 'parent'=>0,'id_proyek'=>2,'status'=>'Belum selesai'],
            ['id_task'=>2, 'nama_task'=>'Task #1', 'tgl_mulai'=>'2017-04-06 00:00:00', 
                'durasi'=>4, 'progress'=>0.5, 'parent'=>1,'id_proyek'=>2,'status'=>'Belum selesai'],
            ['id_task'=>3, 'nama_task'=>'Task #2', 'tgl_mulai'=>'2017-04-05 00:00:00', 
                'durasi'=>6, 'progress'=>0.7, 'parent'=>1,'id_proyek'=>2,'status'=>'Belum selesai'],
            ['id_task'=>4, 'nama_task'=>'Task #3', 'tgl_mulai'=>'2017-04-07 00:00:00', 
                'durasi'=>2, 'progress'=>0, 'parent'=>1,'id_proyek'=>2,'status'=>'Belum selesai'],
            ['id_task'=>5, 'nama_task'=>'Task #1.1', 'tgl_mulai'=>'2017-04-05 00:00:00', 
                'durasi'=>5, 'progress'=>0.34, 'parent'=>2,'id_proyek'=>2,'status'=>'Belum selesai'],
            ['id_task'=>6, 'nama_task'=>'Task #1.2', 'tgl_mulai'=>'2017-04-11 00:00:00', 
                'durasi'=>4, 'progress'=>0.5, 'parent'=>2,'id_proyek'=>2,'status'=>'Belum selesai'],
            ['id_task'=>7, 'nama_task'=>'Task #2.1', 'tgl_mulai'=>'2017-04-07 00:00:00', 
                'durasi'=>5, 'progress'=>0.2, 'parent'=>3,'id_proyek'=>2,'status'=>'Belum selesai'],
            ['id_task'=>8, 'nama_task'=>'Task #2.2', 'tgl_mulai'=>'2017-04-06 00:00:00', 
                'durasi'=>4, 'progress'=>0.9, 'parent'=>3,'id_proyek'=>2,'status'=>'Belum selesai']
        ]);

        //
    }
}
